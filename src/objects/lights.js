import * as THREE from 'three'

export default class Lights {
    constructor(scene){
        this.scene = scene
        this.d = 100
        this.dirLight = new THREE.DirectionalLight(0x666666, 0.54)
        this.ambientLight = new THREE.AmbientLight(0xffffff, 1)
    }
    
    init(){
        this.dirLight.castShadow = true
        this.dirLight.position.set(-8, 12, 8)
        this.dirLight.shadowDarkness = 0.5
        this.dirLight.shadow.mapSize = new THREE.Vector2(2048, 2048)
        this.dirLight.shadow.camera.near = 0.1
        this.dirLight.shadow.camera.far = 1500
        this.dirLight.shadow.camera.left = this.d * -1
        this.dirLight.shadow.camera.right = this.d
        this.dirLight.shadow.camera.top = this.d
        this.dirLight.shadow.camera.bottom = this.d * -1
        this.dirLight.shadow.bias = 0.5
        this.scene.add(this.dirLight)
        this.scene.add(this.ambientLight)
        const helper = new THREE.CameraHelper(this.dirLight.shadow.camera);
        this.scene.add(helper);
    }
}