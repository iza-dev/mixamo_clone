import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import LoadingBar from '../utils/LoadingBar'


class Model {
  constructor () {
    this.loadingBar = new LoadingBar()
    this.dracoloader = new DRACOLoader()
    this.dracoloader.setDecoderPath('./draco/')
    this.gltf
  }
  
  init(obj){
    this.loader = new GLTFLoader(this.loadingBar.loadingBar)
    this.loader.setDRACOLoader(this.dracoloader)
    return new Promise((resolve, reject) => {
      this.loader.load(obj.file, gltfLoaded =>{
        gltfLoaded.name = obj.name
        console.log("Done loading model", gltfLoaded.name);
        resolve(gltfLoaded)
      })
    })
  }
}

export default Model
