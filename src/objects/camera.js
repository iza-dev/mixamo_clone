import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

export default class Camera {
    constructor(obj){
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
        this.scene = obj.scene
        this.canvas = obj.canvas
    }

    init(){
        this.camera.position.set(-2, 2, 2.5)
        this.scene.add(this.camera)

        /**
         * Controls
         */
        const controls = new OrbitControls(this.camera, this.canvas);
        controls.target.set(-2,  0.5, 0);
        controls.update();
    }
}