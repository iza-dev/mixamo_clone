import * as THREE from 'three'
import './style.css'
import Upload from './utils/Upload'
import Model from './objects/model'
import Camera from './objects/camera'
import Lights from './objects/lights'
import Skinify from './utils/Skinify'

export default class AppRender {
  constructor(){

    /**
    * Base
    */
    // Html
    this.canvas = document.querySelector('.webGL')
    this.upload = new Upload()
    //à lancer qd le chargement de la scene est faite
    this.upload.event()
    // Scene
    this.scene = new THREE.Scene()
    this.scene.background = new THREE.Color( 0xf0f0f0 )
    // Models
    this.models =
    [
      {
        name:'source',
        file_path:'jollen.glb'
      },
      {
        name:'target',
        file_path:'jollen_empty.glb'
      }
    ]
    this.meshToSkinnedScene
    //Retargeting
    this.meshSource
    this.meshTarget
    this.skinify
    this.skeleton
    // Animations
    this.animations = []
    this.mixer = null
    this.action 

    /**
     * Camera
     */
    // Base camera and controls
    this.camera = new Camera({scene : this.scene, canvas : this.canvas})
    this.camera.init()
    
    /**
     * Sizes
     */
    this.sizes = {
      width: window.innerWidth,
      height: window.innerHeight
    }
    
    window.addEventListener('resize', () => {
      // Update sizes
      this.sizes.width = window.innerWidth
      this.sizes.height = window.innerHeight
      
      // Update camera
      this.camera.camera.aspect = this.sizes.width / this.sizes.height
      this.camera.camera.updateProjectionMatrix()
      
      // Update renderer
      this.renderer.setSize(this.sizes.width, this.sizes.height)
      this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    })
    
    /**
     * Renderer
     */
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas, antialias: true
    })
    this.renderer.setSize(this.sizes.width, this.sizes.height)
    this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    this.renderer.shadowMap.enabled = true
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap

    this.clock = new THREE.Clock()
    this.previousTime = 0
    
    /**
     * Lights
     */
    this.lights = new Lights(this.scene)
    this.lights.init()
    
    /**
     * Floor
     */
    this.floor = new THREE.Mesh(
      new THREE.PlaneGeometry(5000, 5000, 1, 1),
      new THREE.MeshPhongMaterial({
        color: 0x77b5fe,
        shininess: 0,
      })
      )
      this.floor.castShadow = false
      this.floor.receiveShadow = true
      this.floor.rotation.x = -0.5 * Math.PI
      this.scene.add(this.floor)

      this.model = new Model()
      this.loadModels()
        .catch(error=>{console.error(error)})
        .then(()=>{
          console.log('Meshes are finished to load')
          this.skinify = new Skinify({ meshTarget: this.meshTarget, meshSource: this.meshSource, skeleton: this.skeleton, scene: this.scene})
        })
      
      this.render()
  }

  async loadModels(){
    const modelsSceneArray = this.models.map((modelToLoad)=>{
      return this.model.init({ file: modelToLoad.file_path, name: modelToLoad.name })
    })

    for (const modelScene of modelsSceneArray){
      let resultScene = await modelScene
      if(resultScene.animations.length > 0)
        this.animations = resultScene.animations
      this.mesh_instantiate(resultScene)
    }
  }
  
  mesh_instantiate(gltf){
    if(gltf.name === 'source') this.scene.add(gltf.scene)

    this.traverse_gltf(gltf.scene)
  }
  
  traverse_gltf(meshScene){
    meshScene.traverse(obj => { 
      if (obj.isMesh && !obj.isSkinnedMesh) {
        this.meshTarget = obj
        obj.castShadow = true
        obj.receiveShadow = true
      }
      else if(obj.isMesh && obj.isSkinnedMesh){ 
        this.meshSource = obj
        meshScene.position.x = -2
        meshScene.castShadow = true
        meshScene.receiveShadow = true

        this.mixer = new THREE.AnimationMixer(meshScene)
        this.action = this.mixer.clipAction(this.animations[2]).play()
    
        const skeletonHelper = new THREE.SkeletonHelper(meshScene)
        this.scene.add( skeletonHelper )

        this.skeleton = this.meshSource.skeleton.clone()
      }
      // else if (obj.isBone) {
      //   this.bones.push(obj)
      // }
    })
  }

  render = () => {
    const elapsedTime = this.clock.getElapsedTime()
    const deltaTime = elapsedTime - this.previousTime
    this.previousTime = elapsedTime
    if (this.mixer !== null) { this.mixer.update(deltaTime) }
  
    this.renderer.render(this.scene, this.camera.camera)
    window.requestAnimationFrame(this.render)
  }
  
}