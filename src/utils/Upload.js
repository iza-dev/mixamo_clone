export default class Upload{

  constructor(){
    this.form = document.getElementById('myForm')
    this.inpFile = document.getElementById('inpFile')
    this.btnUpload = document.getElementById('btnUpload')
    this.errorUpload = document.getElementById('errorUpload')
    this.errorUpload.style.visibility = 'hidden'
    this.btnUpload.disabled = true
  }

  event(){
    this.inpFile.addEventListener('change', event => {
      const filename = this.inpFile.files[0].name
      const extensionName = filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename

      if(extensionName === 'glb' || extensionName === 'gltf')
      {
        this.btnUpload.disabled = false
        this.errorUpload.style.visibility = 'hidden'
      }else{
        this.btnUpload.disabled = true
        this.errorUpload.innerHTML = `Upload error! .${extensionName} is not supported.`
        this.errorUpload.style.visibility = 'visible'
        throw new Error('Error type file')
      }

    })
  }

}