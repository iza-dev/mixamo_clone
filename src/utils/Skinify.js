import * as THREE from 'three'

export default class Skinify{
    constructor(obj){
        this.meshTarget = obj.meshTarget
        this.vertexIndex
        this.geometry = this.meshTarget.geometry


        // this.dist = []
        // this.sizingBones = {
        //     segmentHeight: 1,
        //     segmentCount: obj.bones.length,
        //     height: obj.bones.length * 1,
        //     halfHeight: obj.bones.length * 0.5
        // } 
        this.scene = obj.scene
        this.meshSource = obj.meshSource
        this.skeleton = obj.skeleton
        this.skinIndex = this.meshSource.geometry.attributes.skinIndex
        this.skinWeight = this.meshSource.geometry.attributes.skinWeight
        // this.bones = obj.bones
        // this.animations = obj.animations
        this.position = this.geometry.attributes.position
        // this.mixer = null
        // console.log(this.meshToSkin.geometry)
        // this.vertex = new THREE.Vector3()
    //     this.vertex.fromBufferAttribute( this.position, index )
    //     const y = ( this.vertex.y + this.sizingBones.halfHeight )
    
    //     const skinIndex = Math.floor( y / this.sizingBones.segmentHeight )
    //     const skinWeight = ( y % this.sizingBones.segmentHeight ) / this.sizingBones.segmentHeight
    
    //     this.skinIndices.push( skinIndex, skinIndex + 1, 0, 0 );
    //     this.skinWeights.push( 1 - skinWeight, skinWeight, 0, 0 );
    // }
    // // console.info('vertices=' + this.position.count)
    // // console.log('meshToSkin  ',this.meshToSkin)
    // this.meshToSkin.geometry.setAttribute("skinIndex", new THREE.Uint16BufferAttribute(this.skinIndices, 4))
    // this.meshToSkin.geometry.setAttribute("skinWeight", new THREE.Float32BufferAttribute(this.skinWeights, 4))
    
    // const material = new THREE.MeshPhongMaterial( {
        //     color: 0x77b5fe,
        //     emissive: 0x77b5fe,
        //     side: THREE.DoubleSide,
        //     flatShading: true
        // } )
        
        // const mesh = new THREE.SkinnedMesh( this.meshToSkin.geometry, material )
        // const skeleton = new THREE.Skeleton( this.bones )
        // // console.log(skeleton.bones[ 0 ])
        // // skeleton.bones[ 0 ].rotation.x = 0.5;
        // // skeleton.bones[ 0 ].scale.x = 0.01;
        // // skeleton.bones[ 0 ].scale.y = 0.01;
        // // skeleton.bones[ 0 ].scale.z = 0.01;
        
        // // skeleton.bones[ 0 ].position.z = 0;
        // // skeleton.bones[ 0 ].position.y = 0;
        // // skeleton.bones[ 0 ].position.x = 0;
        // // // see example from THREE.Skeleton
        
        // const rootBone = skeleton.bones[ 0 ]
        // this.meshToSkin.add( rootBone )
        
        // // // bind the skeleton to the mesh
        // mesh.name = 'newMesh'
        // mesh.bind( skeleton );
        // // console.log('mesh', this.meshToSkin)
        // // console.log('this.meshSkinned    =>',this.meshSkinned.children[0].children[0])
        // this.animate()
        this.dispatchDataGeometry()
    }
    
    dispatchDataGeometry(){
        console.log('Mesh source')
        console.log(this.meshSource.geometry.attributes)
        console.log('Mesh target')
        console.log(this.meshTarget.geometry.attributes)
        this.meshTarget.geometry.setAttribute('skinIndex', this.skinIndex)
        this.meshTarget.geometry.setAttribute('skinWeight', this.skinWeight)
        this.createSkinnedMesh()
    }

    createSkinnedMesh(){
        const material = new THREE.MeshPhongMaterial({
            color: 0x77b5fe,
            emissive: 0x77b5fe,
            side: THREE.DoubleSide,
            flatShading: true
        })
        const mesh = new THREE.SkinnedMesh( this.meshTarget.geometry, material )
        console.log(mesh)
        // console.log(this.meshSource.bindMatrix)
        mesh.position.x = 2
        // this.scene.add(mesh)

        const skeletonHelper = new THREE.SkeletonHelper(mesh)
        this.scene.add( skeletonHelper )
    }
    
    animate(){
        this.mixer = new THREE.AnimationMixer(this.meshToSkin)
        console.log('this.scene    =>',this.scene)
        this.action = this.mixer.clipAction(this.animations[2])
        this.action.play()
    }
}