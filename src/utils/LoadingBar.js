import * as THREE from 'three'
import { gsap } from 'gsap'

export default class LoadingBar {
  constructor () {
    this.loadingBarElement = document.getElementById('loading-bar')
    this.loadingBar
    this.init()
  }
  
  init(){
    this.loadingBar = new THREE.LoadingManager(
      () =>{
        gsap.delayedCall(0.1, ()=>{
          this.loadingBarElement.classList.add('ended')
          this.loadingBarElement.style.transform = ''
        })
      },
      ( itemUrl, itemsLoaded, itemsTotal) =>{
        const progressRatio = itemsLoaded/itemsTotal
        this.loadingBarElement.style.transform = `scaleX(${progressRatio})`
      }
    )
  }

}
