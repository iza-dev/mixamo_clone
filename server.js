const express = require('express')
const multer = require('multer')
const http = require('http')
const uuid = require('uuid').v4
const app = express()
const port = 3000

const server = http.createServer(app)
const storage = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, 'dist/upload/')
  },
  filename: (req, file, cb) =>{
    const { originalname } = file
    cb(null, originalname)
  }
})

const upload = multer({ storage })

app
  .use(express.static(__dirname + '/dist'))
  .get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
  })
  .post('/upload', upload.single('inpFile'), (req, res) => {
    return res.json({ status: 'OK'})
  })
  .listen(port, () => {
    console.log(`> http://localhost:${port}`)
  })
